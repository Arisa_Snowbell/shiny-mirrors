<a href="https://repology.org/project/shiny-mirrors/versions">
    <img src="https://repology.org/badge/vertical-allrepos/shiny-mirrors.svg" alt="Packaging Status" align="right">
</a>

[![pipeline status](https://gitlab.com/Arisa_Snowbell/shiny-mirrors/badges/domina/pipeline.svg)](https://gitlab.com/Arisa_Snowbell/shiny-mirrors/-/commits/domina)
[![dependency status](https://deps.rs/repo/gitlab/Arisa_Snowbell/shiny-mirrors/status.svg)](https://deps.rs/repo/gitlab/Arisa_Snowbell/shiny-mirrors)
[![total lines](https://tokei.rs/b1/gitlab/Arisa_Snowbell/shiny-mirrors)](https://gitlab.com/Arisa_Snowbell/shiny-mirrors)
[![License:GPL-3.0](https://img.shields.io/badge/license-GPL--3.0-yellow)](https://opensource.org/licenses/GPL-3.0)

Shiny Mirrors
=============



Description
-----------

An alternative to Manjaro's pacman-mirrors and Arch's reflector, written in Rust!
A tool to find the best mirrors for you!

Project Members
---------------

- [Arisa Snowbell](https://gitlab.com/Arisa_Snowbell)

Thanks to | Related Projects
----------------------------

- [pacman-mirrors](https://gitlab.manjaro.org/applications/pacman-mirrors)
- [reflector](https://xyne.dev/projects/reflector)
- [rate-mirrors](https://github.com/westandskif/rate-mirrors)

License
-------
The license is `GNU General Public License v3.0`
