% SHINY-MIRRORS(1)
% Arisa Snowbell (Hyena)
% April 2022

# NAME

shiny-mirrors

# DESCRIPTION

**shiny-mirrors** is a alternative to Manjaro's **pacman-mirrors** and Arch's **reflector**, written in Rust!
A tool to find the best mirrors for you!

# NOTICE!

The first time running shiny-mirrors has to be with root permissions

# SUBCOMMANDS AND THEIR ARGS

**shiny-mirrors** :subcommand: :args_or_subcommand:

## Config

Settings manipulation and magic

- **CLI**

All of CLI arguments can be supplied without value so it removes the current value in config file

**\--continent-value**
:   Writes the supplied continent into the settings file
[possible values: Europe, Africa, Asia, North America, South America, Oceania]

**\--country-value**
:   Writes the supplied countries into the settings file

**\--branch-value**
:   Writes the supplied branch into the settings file
(Changes the branch even in the mirror list file)
[possible values: Stable, Testing, Unstable]

**\--protocol-value**
:   Writes the supplied protocols into the settings file
[possible values: Http, Https, Ftp, Ftps]

**\--help**,             **\-h**
:   Prints help information

- **TUI**

**\--branch**,           **\-b**
:   Set a branch to filter with
[Changes the branch even in the mirror list file]

**\--continent**,        **\-C**
:   Set a continent to filter with

**\--country**,          **\-c**
:   Set a list of countries to filter with

**\--protocol**,        **\-p**
:   Set a list of protocols to filter with

**\--setup**,            **\-s**
:   Set all settings options

### Config Show

Show the settings

## Refresh

Refresh the mirror list

**\--dry-run**,         **\-d**
:   Don't actually write the found mirrors to a local mirror list

**\--file-size**,       **\-f**
:   Size of a file the speed measured is with
[default: Small]
[possible values: Big, Medium, Small]

**\--help**,            **\-h**
:   Prints help information

**\--interactive**,     **\-i**
:   Interactively select which mirrors you want to write to local mirror list

**\--last-sync**,       **\-l**
:   Maximum last sync duration (Hours, Int)
[default: 24]

**\--max**,             **\-m**
:   Maximum of mirrors to write to local mirror list

**\--measure-method**,  **\-M**
:   If to measure only transfer time or total time
[default: Total]
[possible values: Total, Transfer]

**\--timeout**,         **\-t**
:   Time before a benchmark of each mirror timeouts (Seconds, Float)
[default: 3]

**\--updated-only**,    **\-u**
:   Find mirrors which are uptodate, ignore last sync

**\--ipv**,  **\-p**
:   Which IP version the mirror has to support
[possible values: v4, v6]

**\--limit**,  **\-L**
:   Rank only N most recently synced mirrors

## Status

Show the status of mirrors you are using

# DEFAULT BEHAVIOR

## Filtering based on the origin of the mirror

If either continent or country is set in the settings it will use that to filter them, If not it uses GEOAPI service to get the location based on the public address endpoint If the country got by either method does not have mirrors it finds in which continent the country is and filters it which should be bulletproof if I didn't miss some country/continent while making this tool

Global has a special behaviour, if no global mirrors are found it just shows the user it couldn't find any, it won't attempt to find mirrors based geolocation retrieved by the GEOAPI

## Filtering based on speed and last_sync time and status of branches

Either the branch you are on has to be updated or the mirror has to be last time synced in 24 hours otherwise they are filtered out.

After speed ranking of the mirrors it finds the average speed of all the tested mirrors (Excluding which failed completely) and filters mirrors which are above average speed time with a margin error of 200ms

# EXAMPLES

Arisa\'s favourite: sudo shiny-mirrors refresh -f medium -M transfer

shiny-mirrors status / shiny-mirrors
:   Show the status of mirrors you are using

sudo shiny-mirrors config \--branch
:   Open TUI to change the branch

sudo shiny-mirrors refresh
:   Generate a mirror list for Manjaro Linux with the default behaviour

shiny-mirrors config / shiny-mirrors config show
:   Show the settings it was able to parse from the settings file

# EXAMPLE CONF (/etc/shiny-mirrors.conf)

\ \
\##\
\## /etc/shiny-mirrors.conf\
\##\
\ \
\## Separator is \',' for multiple values\
\ \
\## Branch which should shiny-mirrors filter by\
\## Available options: (Stable, Testing, Unstable)\
\## Only one value allowed\
Branch = \
\ \
\## !! NOT RECOMMENDED TO USE !!\
\## Define protocols and priority\
\## Empty means all protocols and by default mirrors url protocol\
\## Available options: (http, https, ftp, ftps)\
\## Multiple values allowed\
Protocol = \
\ \
\## If both Country and Continent is empty shiny-mirrors are going to use GeoAPI service for finding location \
\ \
\## !! RECOMMENDED TO LEAVE EMPTY !!\
\## Define countries\
\## Multiple values allowed\
Country = \
\ \
\# Countries have higher priority than Continent!\
\ \
\## Define the continent\
\## Available options: (Europe, Africa, Asia, North America, South America, Oceania)\
\## Only one value allowed\
Continent = \
\ \
\## Block specific domains aka mirrors if you have some problem with the mirrors\
\## Example: mirror.example.com, sub_domain.domain.top_level_domain\
\## The provided URL's need to contain domain but also subdomain, for example ^\
\## Multiple values allowed\
Blocklist = \
\ \

# REPORTING BUGS

https://gitlab.com/Arisa_Snowbell/shiny-mirrors/issues

# THANKS TO | RELATED PROJECTS

[pacman-mirrors](https://gitlab.manjaro.org/applications/pacman-mirrors)

[reflector](https://xyne.dev/projects/reflector)

[rate-mirrors](https://github.com/westandskif/rate-mirrors)
