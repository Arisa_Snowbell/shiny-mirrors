use std::{
    fs::{DirBuilder, OpenOptions},
    path::Path,
};

use clap::CommandFactory;
use clap_complete::{
    generate,
    shells::{Bash, Elvish, Fish, Zsh},
};

// use clap_mangen::Man;
use cli::Cli;

#[path = "src/cli.rs"]
mod cli;

fn main() {
    let out_dir = Path::new("../target/gen");

    if !out_dir.exists() {
        DirBuilder::new().recursive(true).create(&out_dir).unwrap();
    }

    let mut app = Cli::command();

    // FISH
    let mut fish_file = OpenOptions::new()
        .create(true)
        .write(true)
        .append(false)
        .open(out_dir.join("shiny-mirrors").with_extension("fish"))
        .unwrap();
    generate(Fish, &mut app, "shiny-mirrors", &mut fish_file);

    // ZSH
    let mut zsh_file = OpenOptions::new()
        .create(true)
        .write(true)
        .append(false)
        .open(out_dir.join("_shiny-mirrors"))
        .unwrap();
    generate(Zsh, &mut app, "shiny-mirrors", &mut zsh_file);

    // BASH
    let mut bash_file = OpenOptions::new()
        .create(true)
        .write(true)
        .append(false)
        .open(out_dir.join("shiny-mirrors").with_extension("bash"))
        .unwrap();
    generate(Bash, &mut app, "shiny-mirrors", &mut bash_file);

    // Elvish
    let mut elvish_file = OpenOptions::new()
        .create(true)
        .write(true)
        .append(false)
        .open(out_dir.join("shiny-mirrors").with_extension("elv"))
        .unwrap();
    generate(Elvish, &mut app, "shiny-mirrors", &mut elvish_file);

    // Manual page
    // let mut manual_page = OpenOptions::new()
    // 	.create(true)
    // 	.write(true)
    // 	.append(false)
    // 	.open(out_dir.join("shiny-mirrors").with_extension("1"))
    // 	.unwrap();

    // app._build_all();
    // for command in app.get_subcommands() {
    // 	let name = command.get_name();
    // 	if name == "help" {
    // 		continue;
    // 	}
    // 	let mut manual_page = OpenOptions::new()
    // 		.create(true)
    // 		.write(true)
    // 		.append(false)
    // 		.open(out_dir.join(format!("shiny-mirrors-{name}")).with_extension("1"))
    // 		.unwrap();
    // 	Man::new(command.clone()).render(&mut manual_page).unwrap();
    // }

    // let man = Man::new(app);
    // man.render(&mut manual_page).unwrap();
}
