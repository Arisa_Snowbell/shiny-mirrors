use anyhow::{bail, Result};
use strum::{Display, EnumIter, EnumString};
use thiserror::Error;

pub mod config;

#[derive(EnumIter, EnumString, Display, Debug, PartialEq, Eq, Clone, Copy)]
pub enum SettingsEntry {
    #[cfg(feature = "branch")]
    Branch,
    #[cfg(feature = "country")]
    Country,
    #[cfg(feature = "continent")]
    Continent,
    #[cfg(feature = "protocol")]
    Protocol,
    Blocklist,
}

#[inline]
pub fn is_root() -> Result<()> {
    if !rustix::process::getuid().is_root() {
        bail!(ExitCode::NotRoot);
    }

    Ok(())
}

#[derive(Error, Debug)]
pub enum ExitCode {
    #[error("You need to run this type of action with root permissions!")]
    NotRoot,
    #[cfg(feature = "branch")]
    #[error("You didn't select any {0}.")]
    DidNotChoose(SettingsEntry),
}
