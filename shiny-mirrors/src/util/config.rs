use std::fmt::Display;
#[cfg(feature = "branch")]
use std::io::{Seek, SeekFrom, Write};
use std::{fs::OpenOptions, io::Read, path::PathBuf, str::FromStr};

use anyhow::{bail, Result};
use dialoguer::{console::Term, MultiSelect, Select};
use ini::Ini;
use strum::{IntoEnumIterator, VariantNames};

#[cfg(feature = "branch")]
use shinylib::mirror::distro::{MIRROR_LIST_FILE, REPO_ARCH};
#[cfg(feature = "branch")]
use shinylib::mirror::Branch;
use shinylib::{
    geo::{Continent, Country},
    mirror::Protocol,
    MultiParse, BIN_NAME,
};

use crate::cli::{Cli, Command};

#[cfg(feature = "branch")]
use super::ExitCode;
use super::{is_root, SettingsEntry};

// const SEPARATOR: char = ',';

#[derive(Default, PartialEq, Eq, Clone)]
pub struct Settings {
    #[cfg(feature = "branch")]
    pub branch: Option<Branch>,
    #[cfg(feature = "country")]
    pub countries: Option<Vec<Country>>,
    #[cfg(feature = "continent")]
    pub continent: Option<Continent>,
    #[cfg(feature = "protocol")]
    pub protocols: Option<Vec<Protocol>>,
    pub blocklist: Option<Vec<String>>,
}

impl Display for Settings {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for entry in SettingsEntry::iter() {
            writeln!(f, "{entry}: {}", self.pretty_string_element(entry))?;
        }
        Ok(())
    }
}

// static FIRST_LOADED_SETTINGS: SyncOnceCell<Config> = SyncOnceCell::new();

// impl Drop for Config {
// 	fn drop(&mut self) {
// 		if is_root().is_ok() && Some(&*self) != FIRST_LOADED_SETTINGS.get() {
// 			if self.save().is_err() {
// 				eprintln!("Failed to save the settings!");
// 			}
// 		}
// 	}
// }

pub struct Setup {
    pub settings: Settings,
    #[cfg(feature = "branch")]
    pub branch: Branch,
}

impl Settings {
    fn pretty_string_element(&self, config_entry: SettingsEntry) -> String {
        const SEPARATOR: &str = ", ";

        match config_entry {
            #[cfg(feature = "country")]
            SettingsEntry::Country => {
                if let Some(countries) = &self.countries {
                    return countries.iter().map(Country::to_string).collect::<Vec<String>>().join(SEPARATOR);
                }
            }
            #[cfg(feature = "continent")]
            SettingsEntry::Continent => {
                if let Some(continent) = self.continent {
                    return continent.to_string();
                }
            }
            #[cfg(feature = "protocol")]
            SettingsEntry::Protocol => {
                if let Some(protocols) = &self.protocols {
                    return protocols.iter().map(Protocol::to_string).collect::<Vec<String>>().join(SEPARATOR);
                }
            }
            #[cfg(feature = "branch")]
            SettingsEntry::Branch => {
                if let Some(branch) = self.branch {
                    return branch.to_string();
                }
            }
            SettingsEntry::Blocklist => {
                if let Some(blocklist) = &self.blocklist {
                    return blocklist.join(SEPARATOR);
                }
            }
        }

        String::new()
    }

    //pub fn print_config(&self) {
    //	for entry in SettingsEntry::iter() {
    //		println!("{entry}: {}", self.pretty_string_element(entry));
    //	}
    //}

    pub fn selection_menu(
        &mut self,
        #[cfg(feature = "branch")] branch: bool,
        #[cfg(feature = "country")] countries: bool,
        #[cfg(feature = "continent")] continent: bool,
        #[cfg(feature = "protocol")] protocol: bool,
    ) -> Result<()> {
        is_root()?;

        #[cfg(feature = "branch")]
        if branch {
            self.ask_one(SettingsEntry::Branch)?;
        }

        #[cfg(feature = "country")]
        if countries {
            self.ask_multiple(SettingsEntry::Country)?;
        }

        #[cfg(feature = "continent")]
        if continent {
            self.ask_one(SettingsEntry::Continent)?;
        }

        #[cfg(feature = "protocol")]
        if protocol {
            self.ask_multiple(SettingsEntry::Protocol)?;
        }

        self.save()?;

        Ok(())
    }

    fn smart_default() -> Result<Self> {
        #[allow(unused_mut)]
        let mut settings = Self::default();
        #[cfg(feature = "branch")]
        let _ = settings.set_local_mirror_list_branch_to_settings().is_err();
        Ok(settings)
    }

    pub fn setup(app: &Cli) -> Result<Setup> {
        let mut settings = match Self::load() {
            #[allow(unused_mut)]
            Ok(mut config) => {
                #[cfg(feature = "branch")]
                if config.branch.is_none() && !matches!(app.command, Some(Command::Config { .. })) {
                    is_root()?;
                    config.branch = Self::smart_default()?.branch;
                    config.save()?;
                }
                config
            }
            Err(_) => {
                is_root()?;
                let config = Self::smart_default()?;
                config.save()?;
                config
            }
        };

        // if FIRST_LOADED_SETTINGS.set(settings.clone()).is_err() {
        // 	eprintln!("Failed to init OnceCell in config.rs!");
        // 	unreachable!()
        // }

        #[cfg(any(feature = "branch", feature = "country", feature = "protocol", feature = "continent"))]
        if let Some(Command::Config {
            #[cfg(feature = "branch")]
            branch_value,
            #[cfg(feature = "country")]
            country_value,
            #[cfg(feature = "protocol")]
            protocol_value,
            #[cfg(feature = "continent")]
            continent_value,
            setup,
            #[cfg(feature = "country")]
            country,
            #[cfg(feature = "branch")]
            branch,
            #[cfg(feature = "continent")]
            continent,
            #[cfg(feature = "protocol")]
            protocol,
            subcommand: _,
        }) = &app.command
        {
            #[cfg(feature = "branch")]
            let branch_flag = branch;
            #[cfg(not(feature = "branch"))]
            let branch_flag = &false;

            #[cfg(feature = "country")]
            let country_flag = country;
            #[cfg(not(feature = "country"))]
            let country_flag = &false;

            #[cfg(feature = "continent")]
            let continent_flag = continent;
            #[cfg(not(feature = "continent"))]
            let continent_flag = &false;

            #[cfg(feature = "protocol")]
            let protocol_flag = protocol;
            #[cfg(not(feature = "protocol"))]
            let protocol_flag = &false;

            // Hidden options
            #[cfg(feature = "branch")]
            let branch_value_flag = branch_value.is_some();
            #[cfg(not(feature = "branch"))]
            let branch_value_flag = false;

            #[cfg(feature = "country")]
            let country_value_flag = country_value.is_some();
            #[cfg(not(feature = "country"))]
            let country_value_flag = false;

            #[cfg(feature = "continent")]
            let continent_value_flag = continent_value.is_some();
            #[cfg(not(feature = "continent"))]
            let continent_value_flag = false;

            #[cfg(feature = "protocol")]
            let protocol_value_flag = protocol_value.is_some();
            #[cfg(not(feature = "protocol"))]
            let protocol_value_flag = false;

            if *setup {
                settings.selection_menu(
                    #[cfg(feature = "branch")]
                    true,
                    #[cfg(feature = "country")]
                    true,
                    #[cfg(feature = "continent")]
                    true,
                    #[cfg(feature = "protocol")]
                    true,
                )?;
            } else if branch_value_flag || country_value_flag || continent_value_flag || protocol_value_flag {
                is_root()?;
                #[cfg(feature = "branch")]
                if let Some(branch_value) = branch_value {
                    if let Ok(mut file) = OpenOptions::new().write(true).read(true).create(false).open(MIRROR_LIST_FILE) {
                        settings.branch = match branch_value {
                            Some(new_branch) => {
                                let mut string = String::new();
                                file.read_to_string(&mut string)?;

                                let old_branch = Branch::get_branch_from_local_mirror_list_string(&string);
                                if let Ok(old_branch) = old_branch {
                                    if old_branch != *new_branch {
                                        let string = string.replace(
                                            &format!("/{}/{}", serde_plain::to_string(&old_branch)?, REPO_ARCH.join("/")),
                                            &format!("/{}/{}", serde_plain::to_string(&new_branch)?, REPO_ARCH.join("/")),
                                        );
                                        file.set_len(0)?;
                                        file.seek(SeekFrom::Start(0))?;
                                        file.write_all(string.as_bytes())?;
                                    }
                                }
                                Some(*new_branch)
                            }
                            None => {
                                file.set_len(0)?;
                                None
                            }
                        };
                    }
                }

                #[cfg(feature = "country")]
                if country_value_flag {
                    settings.countries = match country_value {
                        Some(countries) => {
                            if countries.is_empty() {
                                None
                            } else {
                                Some(countries.clone())
                            }
                        }
                        None => None,
                    };
                }

                #[cfg(feature = "continent")]
                if let Some(continent_value) = continent_value {
                    settings.continent = *continent_value;
                }

                #[cfg(feature = "protocol")]
                if protocol_value_flag {
                    settings.protocols = match protocol_value {
                        Some(protocols) => {
                            if protocols.is_empty() {
                                None
                            } else {
                                Some(protocols.clone())
                            }
                        }
                        None => None,
                    };
                }

                settings.save()?;
            } else if *branch_flag || *country_flag || *continent_flag || *protocol_flag {
                settings.selection_menu(
                    #[cfg(feature = "branch")]
                    *branch_flag,
                    #[cfg(feature = "country")]
                    *country_flag,
                    #[cfg(feature = "continent")]
                    *continent_flag,
                    #[cfg(feature = "protocol")]
                    *protocol_flag,
                )?;
            } else {
                println!("{settings}");
            }
        }

        #[cfg(feature = "branch")]
        let branch = match settings.branch {
            Some(branch) => branch,
            None => {
                settings.selection_menu(true, false, false, false)?;
                match settings.branch {
                    Some(branch) => branch,
                    None => bail!(ExitCode::DidNotChoose(SettingsEntry::Branch)),
                }
            }
        };

        Ok(Setup {
            #[cfg(feature = "branch")]
            branch,
            settings,
        })
    }

    #[cfg(any(feature = "protocol", feature = "country"))]
    fn ask_multiple(&mut self, config_entry: SettingsEntry) -> Result<()> {
        let items = match config_entry {
            #[cfg(feature = "country")]
            SettingsEntry::Country => {
                let mut items = Country::VARIANTS[..Country::VARIANTS.len() - 1].to_owned();
                // Doing it this way so the global variant is not sorted by alphabet
                items[1..].sort_unstable();
                items
            }
            #[cfg(feature = "protocol")]
            SettingsEntry::Protocol => Protocol::VARIANTS.to_owned(),
            _ => bail!("Wrong config entry!"),
        };

        let selected_items = MultiSelect::new()
            .with_prompt(format!(
                "Select any {} (Press Enter to continue, Space Bar to select element)",
                config_entry
            ))
            .items(&items)
            .clear(true)
            .interact_on(&Term::stdout())?;

        match config_entry {
            #[cfg(feature = "country")]
            SettingsEntry::Country => {
                let countries = selected_items
                    .into_iter()
                    .filter_map(|i| Country::from_str(items[i]).ok())
                    .collect::<Vec<Country>>();

                if countries.is_empty() {
                    self.countries = None;
                } else {
                    self.countries = Some(countries);
                };
            }
            #[cfg(feature = "protocol")]
            SettingsEntry::Protocol => {
                let protocols = selected_items
                    .into_iter()
                    .filter_map(|i| Protocol::from_str(items[i]).ok())
                    .collect::<Vec<Protocol>>();

                if protocols.is_empty() {
                    self.protocols = None;
                } else {
                    self.protocols = Some(protocols);
                };
            }
            _ => bail!("Wrong config entry!"),
        };

        Ok(())
    }

    #[cfg(any(feature = "continent", feature = "branch"))]
    fn ask_one(&mut self, config_entry: SettingsEntry) -> Result<()> {
        let items = match config_entry {
            #[cfg(feature = "continent")]
            SettingsEntry::Continent => Continent::VARIANTS,
            #[cfg(feature = "branch")]
            SettingsEntry::Branch => Branch::VARIANTS,
            _ => bail!("Wrong config entry!"),
        };

        let selection = Select::new()
            .with_prompt(format!("Select your {config_entry}"))
            .default(0)
            .items(items)
            .clear(true)
            .interact_on_opt(&Term::stdout())?;

        match config_entry {
            #[cfg(feature = "branch")]
            SettingsEntry::Branch => {
                let new_branch = selection.map_or_else(
                    || {
                        println!("You didn't select any branch!");
                        None
                    },
                    |index| Branch::from_str(items[index]).ok(),
                );

                if let Ok(mut file) = OpenOptions::new().write(true).read(true).create(false).open(MIRROR_LIST_FILE) {
                    match new_branch {
                        Some(new_branch) => {
                            let mut string = String::new();
                            file.read_to_string(&mut string)?;

                            let old_branch = Branch::get_branch_from_local_mirror_list_string(&string);
                            if let Ok(old_branch) = old_branch {
                                if old_branch != new_branch {
                                    let string = string.replace(
                                        &format!("/{}/{}", serde_plain::to_string(&old_branch)?, REPO_ARCH.join("/")),
                                        &format!("/{}/{}", serde_plain::to_string(&new_branch)?, REPO_ARCH.join("/")),
                                    );
                                    file.set_len(0)?;
                                    file.seek(SeekFrom::Start(0))?;
                                    file.write_all(string.as_bytes())?;
                                }
                            }
                        }
                        None => {
                            // No branch == no local mirror list
                            file.set_len(0)?;
                        }
                    }
                }

                self.branch = new_branch;
            }
            #[cfg(feature = "continent")]
            SettingsEntry::Continent => {
                let new_continent = selection.map_or_else(|| None, |index| Continent::from_str(items[index]).ok());
                self.continent = new_continent;
            }
            _ => bail!("Wrong config entry!"),
        }

        Ok(())
    }

    fn save(&self) -> Result<()> {
        let config_file = PathBuf::from_str(&format!("/etc/{BIN_NAME}.conf")).expect("The config file path is not valid!");

        let mut ini = Ini::new();

        for entry in SettingsEntry::iter() {
            ini.set_to::<String>(None, entry.to_string(), self.pretty_string_element(entry));
        }

        ini.write_to_file(config_file)?;

        Ok(())
    }

    fn load() -> Result<Self> {
        let config_file = PathBuf::from_str(&format!("/etc/{}.conf", BIN_NAME)).expect("The config file path is not valid!");
        let mut file = OpenOptions::new().read(true).write(false).open(config_file)?;
        let mut buf = String::new();
        file.read_to_string(&mut buf)?;

        let ini = Ini::load_from_str(&buf)?;

        Ok(Self {
            #[cfg(feature = "branch")]
            branch: ini
                .get_from::<String>(None, &SettingsEntry::Branch.to_string())
                .and_then(|v| Branch::from_str(v).ok()),
            #[cfg(feature = "continent")]
            continent: ini
                .get_from::<String>(None, &SettingsEntry::Continent.to_string())
                .and_then(|v| Continent::from_str(v).ok()),
            #[cfg(feature = "country")]
            countries: ini
                .get_from::<String>(None, &SettingsEntry::Country.to_string())
                .and_then(Country::parse_multiple),
            #[cfg(feature = "protocol")]
            protocols: ini
                .get_from::<String>(None, &SettingsEntry::Protocol.to_string())
                .and_then(Protocol::parse_multiple),
            blocklist: ini
                .get_from::<String>(None, &SettingsEntry::Blocklist.to_string())
                .and_then(String::parse_multiple),
        })
    }

    #[cfg(feature = "branch")]
    fn set_local_mirror_list_branch_to_settings(&mut self) -> Result<()> {
        let branch = Branch::get_branch_from_local_mirror_list()?;
        self.branch = Some(branch);
        Ok(())
    }
}
