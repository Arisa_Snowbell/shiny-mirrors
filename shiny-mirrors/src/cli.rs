use clap::{Parser, Subcommand};

#[cfg(feature = "continent")]
use shinylib::geo::Continent;
#[cfg(feature = "country")]
use shinylib::geo::Country;
#[cfg(feature = "branch")]
use shinylib::mirror::Branch;
use shinylib::{
    mirror::{IPv, Protocol},
    {FileSize, MeasureMethod},
};

#[derive(Parser)]
#[clap(version, about)]
#[clap(author = "Arisa Snowbell - Hyena")]
#[clap(infer_subcommands = true)]
pub struct Cli {
    #[clap(subcommand)]
    pub command: Option<Command>,
}

const VALUE_CONFLICT: &[&str] = &[
    #[cfg(feature = "branch")]
    "branch",
    #[cfg(feature = "protocol")]
    "protocol",
    #[cfg(feature = "continent")]
    "continent",
    #[cfg(feature = "country")]
    "country",
];

#[derive(Subcommand)]
pub enum Command {
    #[clap(about = "Get the perfect mirrors for you")]
    Refresh {
        #[clap(short, long, help = "Don't actually write the found mirrors to a local mirror list")]
        dry_run: bool,
        #[cfg(feature = "last_sync")]
        #[clap(short, long, help = "Maximum last sync duration (Hours, Int)", default_value_t = 24u64)]
        last_sync: u64,
        #[cfg(any(feature = "status", feature = "last_sync"))]
        #[clap(short, long, help = "Find mirrors which are uptodate, ignore max last sync")]
        updated_only: bool,
        #[clap(
            value_enum,
            short = 'p',
            long,
            help = "Which IP version the mirror has to support",
            ignore_case = true
        )]
        ipv: Option<IPv>,
        #[clap(short, long, help = "Interactively select which mirrors you want to write to local mirror list")]
        interactive: bool,
        #[clap(short, long, help = "Maximum of mirrors to write to local mirror list")]
        max: Option<usize>,
        #[clap(
            short,
            long,
            help = "Time before a benchmark of each mirror timeouts (Seconds, Float)",
            default_value_t = 3f64
        )]
        timeout: f64,
        #[clap(value_enum, short, long, help = "Size of a file the speed measured is with", default_value_t = FileSize::Small, ignore_case = true)]
        file_size: FileSize,
        #[clap(value_enum, short = 'M', long, help = "If to measure only transfer time or total time", default_value_t = MeasureMethod::Total, ignore_case = true)]
        measure_method: MeasureMethod,
        #[clap(short = 'L', long, help = "Rank only N most recently synced mirrors")]
        limit: Option<usize>,
    },
    #[clap(about = "Show status of mirrors you are using")]
    Status,
    #[cfg(any(feature = "country", feature = "continent", feature = "branch", feature = "protocol"))]
    #[clap(about = "Settings manipulation and magic")]
    Config {
        #[cfg(feature = "protocol")]
        #[clap(value_enum, num_args = 0.., long, ignore_case = true, hide = true, conflicts_with_all = VALUE_CONFLICT)]
        protocol_value: Option<Vec<Protocol>>,
        #[cfg(feature = "branch")]
        #[clap(value_enum, long, ignore_case = true, hide = true, conflicts_with_all = VALUE_CONFLICT)]
        branch_value: Option<Option<Branch>>,
        #[cfg(feature = "country")]
        #[clap(value_enum, num_args = 0.., long, ignore_case = true, hide = true, conflicts_with_all = VALUE_CONFLICT)]
        country_value: Option<Vec<Country>>,
        #[cfg(feature = "continent")]
        #[clap(value_enum, long, ignore_case = true, hide = true, conflicts_with_all = VALUE_CONFLICT)]
        continent_value: Option<Option<Continent>>,
        #[clap(short, long, help = "Set all settings options", conflicts_with_all = & [
        #[cfg(feature = "branch")]
        "branch",
        #[cfg(feature = "protocol")]
        "protocol",
        #[cfg(feature = "continent")]
        "continent",
        #[cfg(feature = "country")]
        "country",
		#[cfg(feature = "branch")]
		"branch_value",
		#[cfg(feature = "country")]
		"country_value",
		#[cfg(feature = "continent")]
		"continent_value",
		#[cfg(feature = "protocol")]
		"protocol_value",
        ])]
        setup: bool,
        #[cfg(feature = "country")]
        #[clap(short, long, help = "Set a list of countries to filter with")]
        country: bool,
        #[cfg(feature = "branch")]
        #[clap(short, long, help = "Set a branch to filter with")]
        branch: bool,
        #[cfg(feature = "continent")]
        #[clap(short = 'C', long, help = "Set a continent to filter with")]
        continent: bool,
        #[cfg(feature = "protocol")]
        #[clap(short, long, help = "Set a list of protocols to filter with")]
        protocol: bool,
        #[clap(subcommand)]
        subcommand: Option<ConfigSubcommand>,
    },
}

#[derive(Subcommand)]
pub enum ConfigSubcommand {
    #[clap(about = "Show the settings")]
    Show,
}
