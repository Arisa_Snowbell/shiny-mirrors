#![warn(clippy::all, clippy::nursery)]
#![forbid(unsafe_code)]
use std::{fs::OpenOptions, io::Write, process::exit, sync::Arc, time::Duration};

use anyhow::{bail, Result};
use clap::Parser;
use dialoguer::{console::Term, MultiSelect};

use cli::{Cli, Command};
use rustix::{
    fs::{flock, unlink, FlockOperation},
    io::Errno,
};
use shinylib::{
    geo::{Continent, Country},
    mirror::MirrorList,
    ShinyError::{self, NoMirrorsFound},
};

use crate::util::{config::Settings, config::Setup, is_root};

mod cli;
mod util;

#[cfg(not(any(
    feature = "manjaro",
    feature = "arch",
    feature = "artix",
    feature = "rebornos",
    feature = "endeavouros"
)))]
compile_error!("At least one distro feature must be enabled!");

// #[cfg(multiple(feature = "manjaro", feature = "arch", feature = "artix", feature = "rebornos", feature = "endeavouros"))]
// compile_error!("You can't compile shiny-mirrors to support multiple distros at the same time! It must be compiled separately!");

const PID_PATH: &str = "/tmp/shiny_mirrors.pid";

fn main() -> Result<()> {
    let mut pid_file = match OpenOptions::new().read(true).write(true).create(true).append(false).open(PID_PATH) {
        Ok(v) => v,
        Err(_) => bail!(ShinyError::AlreadyRunning),
    };

    match flock(&mut pid_file, FlockOperation::NonBlockingLockExclusive) {
        Err(Errno::WOULDBLOCK) => {
            // eprintln!("Application is already running!");
            bail!(ShinyError::AlreadyRunning);
        }
        e => e.unwrap(),
    };

    pid_file
        .write_all(rustix::process::getpid().as_raw_nonzero().to_string().as_bytes())
        .unwrap();
    let pid_file = Arc::new(pid_file);
    let pid_file2 = pid_file.clone();

    ctrlc::set_handler(move || {
        // FIXME: Still don't like this
        unlink(PID_PATH).unwrap();
        flock(&pid_file2, FlockOperation::Unlock).unwrap();
        exit(6);
    })
    .expect("handle");

    let res = inside_main();

    unlink(PID_PATH).unwrap();
    flock(&pid_file, FlockOperation::Unlock).unwrap();

    res
}

fn inside_main() -> Result<()> {
    let app = Cli::parse();

    let Setup {
        #[cfg(feature = "branch")]
        branch,
        settings:
            Settings {
                #[cfg(feature = "branch")]
                    branch: _,
                #[cfg(feature = "country")]
                countries,
                #[cfg(feature = "continent")]
                continent,
                #[cfg(feature = "protocol")]
                protocols,
                blocklist,
            },
    } = Settings::setup(&app)?;

    match app.command {
        // FIXME: I don't like this "function" branch so much
        // Better would be to use guard clauses or something but... well, it's not possible here really, well it is but...
        Some(Command::Refresh {
            dry_run,
            #[cfg(feature = "last_sync")]
            last_sync,
            #[cfg(feature = "status")]
            updated_only,
            ipv,
            interactive,
            max,
            timeout,
            file_size,
            measure_method,
            limit: rank_limit,
        }) => {
            if !dry_run {
                is_root()?;
            }

            let mut mirror_list = MirrorList::download_mirrors()?;
            #[cfg(feature = "last_sync")]
            let last_sync = Duration::from_secs(last_sync * 3_600);

            #[cfg(feature = "status")]
            mirror_list.filter_status(
                #[cfg(feature = "branch")]
                branch,
                updated_only,
                Some(last_sync),
            );

            // If blocklist has some domains filter them from MirrorList
            if let Some(blocklist) = &blocklist {
                mirror_list.filter_domains(blocklist);
            }

            // If some protocols are set filter by them the MirrorList
            #[cfg(feature = "protocol")]
            if let Some(protocols) = &protocols {
                mirror_list.filter_protocol(protocols)?;
            }

            #[cfg(feature = "arch")]
            mirror_list.retain(|x| x.active);

            #[cfg(all(feature = "continent", feature = "country"))]
            let mirror_list_backup = mirror_list.clone();
            #[cfg(all(feature = "continent", feature = "country"))]
            let mut b_continent = false;

            // FIXME: Separate continent and country logic
            // If no geolocation settings get geolocation information based on public ip endpoint and filter by country, if none filter by country's continent
            // else try first filter by values which are in settings
            #[cfg(feature = "country")]
            if continent.is_none() && countries.is_none() {
                println!("No continent or country set, using a geolocation based on your public IP Address endpoint.");
                mirror_list.get_perfect_mirrors(mirror_list_backup)?;
            } else {
                // If some countries in settings
                if let Some(countries) = &countries {
                    mirror_list.filter_country(countries);

                    // If the country has no mirrors and the country is not global
                    if mirror_list.iter().filter(|mirror| mirror.country != Country::Global).count() == 0
                        && !countries.contains(&Country::Global)
                    {
                        println!("The selected countries very likely don't have any mirrors.");
                        mirror_list = mirror_list_backup.clone();

                        // if continent is none so we can't filter by continent further down, do geolocation
                        // else set country to None because it has no mirrors so further down it will use the continent thats stored in settings
                        if continent.is_none() {
                            println!("Getting mirrors based on a country which was acquired by geolocation.");
                            let country = Country::find_current_country().unwrap_or(Country::Unknown);
                            mirror_list.filter_country(&[country]);

                            // Again couldn't find a mirrors for country that was retrieved by geolocation so filter by geolocated country's continnent
                            if mirror_list.iter().filter(|mirror| mirror.country != Country::Global).count() == 0 {
                                println!("Couldn't get any mirrors based on country gotten by the geolocation.\nGetting mirrors based on a continent which was acquired by geolocation.");
                                mirror_list = mirror_list_backup;

                                mirror_list.filter_continent(if country != Country::Unknown {
                                    country.get_continent()
                                } else {
                                    Continent::find_current_continent()?
                                });
                            }
                        } else {
                            b_continent = true;
                            println!("Using continent from the settings.");
                        }
                    }
                }

                // If no country in settings or has no mirrors and continent is some, filter by continent
                if countries.is_none() || b_continent {
                    if let Some(continent) = continent {
                        println!("Getting mirrors based on a continent which was acquired from settings.");
                        mirror_list.filter_continent(continent);
                    }
                }
            }

            #[cfg(all(feature = "continent", not(feature = "country")))]
            if let Some(continent) = continent {
                println!("Getting mirrors based on a continent which was acquired from settings.");
                mirror_list.filter_continent(continent);
            } else {
                if let Ok(continent) = Continent::find_current_continent() {
                    println!("Getting mirrors based on a continent which was acquired from geolocation.");
                    mirror_list.filter_continent(continent);
                }
            }

            if let Some(ipv) = ipv {
                mirror_list.filter_ipv(ipv);
            }

            // If interactive do {menu selection}
            // else {find fastest mirrors}
            if interactive {
                let selected_items = MultiSelect::new()
                    .with_prompt("Select any Mirror (Press Enter to continue, Space Bar to select element)")
                    .items(&mirror_list.iter().map(|m| m.url.to_string()).collect::<Vec<String>>())
                    .clear(true)
                    .interact_on(&Term::stdout())?;

                if selected_items.is_empty() {
                    println!("You didn't choose any mirror!");
                    mirror_list.clear();
                } else {
                    let cloned_mirror_list = mirror_list.clone();
                    for (index, mirror1) in cloned_mirror_list.iter().enumerate() {
                        if !selected_items.contains(&index) {
                            if let Some(index) = mirror_list.iter().position(|mirror2| mirror2.url == mirror1.url) {
                                mirror_list.remove(index);
                            }
                        }
                    }
                }
            } else {
                mirror_list.filter_top_fastest_mirrors(
                    #[cfg(feature = "branch")]
                    branch,
                    max,
                    Some(Duration::from_secs_f64(timeout)),
                    Some(file_size),
                    Some(measure_method),
                    rank_limit,
                )?;

                if mirror_list.is_empty() {
                    bail!(NoMirrorsFound);
                }
            }

            if !dry_run {
                mirror_list.write_to_local_mirror_list(
                    #[cfg(feature = "branch")]
                    branch,
                )?;
            }
        }
        Some(Command::Config { .. }) => {},
        _ => {
            MirrorList::print_status(
                #[cfg(feature = "branch")]
                branch,
            )?;
        }
    }

    Ok(())
}
