use chrono::Local;
use serde::{Deserialize, Deserializer, Serialize};
use smallvec::{smallvec, SmallVec};
use std::{
    ops::{Deref, DerefMut},
    time::Duration,
};
use url::Url;

use crate::{geo::Country, mirror::Status, USER_AGENT};

use super::{Protocol, ShinyResult};

pub const DISTRO_NAME: &str = "Arch";

pub const REPO_ARCH: [&str; 3] = ["$repo", "os", "$arch"];
pub const MIRRORLIST_URL: &str = "https://archlinux.org/mirrors/status/json/";
pub const MIRROR_LIST_FILE: &str = "/etc/pacman.d/mirrorlist";

// 6.10.2021 Around 176KB
pub const SMALL_TEST_FILE: &str = "core.db.tar.gz";
pub const SMALL_TEST_FILE_DIR: [&str; 2] = ["core", "os"];

// 2.7.2023 Around 8.6MB
// Uses the BIG_TEST_FILE_DIR
pub const MEDIUM_TEST_FILE: &str = "extra.db.tar.gz";
pub const MEDIUM_TEST_FILE_DIR: [&str; 2] = BIG_TEST_FILE_DIR;

// 2.7.2023 Around 40MB
pub const BIG_TEST_FILE: &str = "extra.files.tar.gz";
pub const BIG_TEST_FILE_DIR: [&str; 2] = ["extra", "os"];

/// List of [mirrors][Mirror]
#[derive(Default, Clone, Debug, Serialize, Deserialize)]
pub struct MirrorList {
    urls: Vec<Mirror>,
}

impl MirrorList {
    /// Download the status of mirror list
    pub fn pure_download_mirrors() -> ShinyResult<Self> {
        let mirrors: Self = reqwest::blocking::Client::builder()
            .user_agent(USER_AGENT)
            .build()?
            .get(MIRRORLIST_URL)
            .send()?
            .json()?;

        let mut mirror_urls = mirrors.urls;

        // Find all same domains and get all protocols into one entry
        let mut mirror_list = Self::default();
        for curr_mirror in mirror_urls.clone().into_iter() {
            if mirror_urls.iter().any(|mirror| mirror.url.domain() == curr_mirror.url.domain()) {
                let protocols: SmallVec<[Protocol; 5]> = mirror_urls
                    .iter()
                    .filter(|mirror| mirror.url.domain() != curr_mirror.url.domain())
                    .map(|mirror| mirror.protocols[0])
                    .collect();
                mirror_urls.retain(|mirror| mirror.url.domain() != curr_mirror.url.domain());
                mirror_list.push(Mirror { protocols, ..curr_mirror })
            }
        }

        Ok(mirror_list)
    }
}

impl Deref for MirrorList {
    type Target = Vec<Mirror>;

    fn deref(&self) -> &Vec<Mirror> {
        &self.urls
    }
}

impl DerefMut for MirrorList {
    fn deref_mut(&mut self) -> &mut Vec<Mirror> {
        &mut self.urls
    }
}

/// Mirror entry in [Mirror List][MirrorList]
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Mirror {
    pub url: Url,
    pub country: Country,
    /// None == Not measured or failed to connect or such
    #[serde(skip)]
    pub time: Option<Duration>,
    /// None == Unknown | Couldn't parse
    #[serde(deserialize_with = "parse_last_sync")]
    pub last_sync: Option<Duration>,
    #[serde(deserialize_with = "parse_protocol")]
    #[serde(rename = "protocol")]
    pub protocols: SmallVec<[Protocol; 5]>,
    pub completion_pct: Option<f64>,
    pub delay: Option<isize>,
    pub ipv6: bool,
    pub ipv4: bool,
    pub active: bool,
}

impl Mirror {
    pub fn get_status(&self) -> Status {
        if self.completion_pct == Some(1f64) && self.last_sync.map_or(false, |sync| sync <= Duration::from_secs(3_600)) {
            Status::Good
        } else {
            Status::Bad
        }
    }
}

fn parse_protocol<'de, D>(deserializer: D) -> Result<SmallVec<[Protocol; 5]>, D::Error>
where
    D: Deserializer<'de>,
{
    let item: Protocol = Deserialize::deserialize(deserializer)?;

    Ok(smallvec![item])
}

fn parse_last_sync<'de, D>(deserializer: D) -> Result<Option<Duration>, D::Error>
where
    D: Deserializer<'de>,
{
    Ok(Deserialize::deserialize(deserializer)
        .ok()
        .and_then(|datetime: chrono::DateTime<Local>| Local::now().signed_duration_since::<Local>(datetime).to_std().ok()))
}
