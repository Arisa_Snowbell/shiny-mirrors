use chrono::{DateTime, Local};
use serde::{Deserialize, Deserializer, Serialize};
#[cfg(any(feature = "branch", feature = "protocol"))]
use smallvec::SmallVec;
use std::{
    ops::{Deref, DerefMut},
    str::FromStr,
    time::Duration,
};
use url::Url;

use crate::{
    geo::{Continent, Country},
    mirror::Status,
    USER_AGENT,
};

use super::{Protocol, Result};

pub const DISTRO_NAME: &str = "Artix";

pub const REPO_ARCH: [&str; 3] = ["$repo", "os", "$arch"];
pub const MIRRORLIST_URL: &str = "https://gitea.artixlinux.org/packagesA/artix-mirrorlist/raw/branch/master/trunk/mirrorlist";
pub const MIRROR_LIST_FILE: &str = "/etc/pacman.d/mirrorlist";

// 30.11.2021 Around 281KB
pub const SMALL_TEST_FILE: &str = "system.db.tar.gz";
pub const SMALL_TEST_FILE_DIR: [&str; 2] = ["system", "os"];

// 30.11.2021 Around 2.1MB
// Uses the BIG_TEST_FILE_DIR
pub const MEDIUM_TEST_FILE: &str = "galaxy.db.tar.gz";
pub const MEDIUM_TEST_FILE_DIR: [&str; 2] = BIG_TEST_FILE_DIR;

// 30.11.2021 Around 5.3MB
pub const BIG_TEST_FILE: &str = "galaxy.files.tar.gz";
pub const BIG_TEST_FILE_DIR: [&str; 2] = ["galaxy", "os"];

/// List of [mirrors][Mirror]
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct MirrorList(Vec<Mirror>);

impl MirrorList {
    /// Download the status of mirror list
    pub fn pure_download_mirrors() -> Result<Self> {
        let v = reqwest::blocking::Client::builder()
            .user_agent(USER_AGENT)
            .build()?
            .get(MIRRORLIST_URL)
            .send()?
            .text()?;

        let mut mirrorlist = Self(Vec::new());

        let mut current_location = None;

        for line in v.lines() {
            match line {
                line if line.starts_with("Server = ") => {
                    let line = line
                        .trim_start_matches("Server = ")
                        .trim()
                        .trim_end_matches(REPO_ARCH.join("/").as_str());
                    if let Some(continent) = current_location {
                        let m = Mirror {
                            url: Url::parse(line)?,
                            continent,
                            time: None,
                        };
                        mirrorlist.push(m);
                    }
                }
                line if line.starts_with("## ") => {
                    let line = line.trim_start_matches("##").trim();
                    if let Ok(continent) = Continent::from_str(line) {
                        current_location = Some(continent);
                    }
                }
                _ => {}
            }
        }
        Ok(mirrorlist)
    }
}

impl Deref for MirrorList {
    type Target = Vec<Mirror>;

    fn deref(&self) -> &Vec<Mirror> {
        &self.0
    }
}

impl DerefMut for MirrorList {
    fn deref_mut(&mut self) -> &mut Vec<Mirror> {
        &mut self.0
    }
}

/// Mirror entry in [Mirror List][MirrorList]
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Mirror {
    pub url: Url,
    pub continent: Continent,
    /// None == Not measured or failed to connect or such
    #[serde(skip)]
    pub time: Option<Duration>,
}
