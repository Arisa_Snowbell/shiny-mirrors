use std::{
    env::consts::ARCH,
    fs::OpenOptions,
    io::{stdout, Read, Write},
    net::SocketAddr,
    ops::Add,
    str::FromStr,
    time::{Duration, Instant},
};

use chrono::Local;
use clap::ValueEnum;
use colored::Colorize;
use serde::{self, Deserialize, Serialize};
use serde_repr::{Deserialize_repr, Serialize_repr};
use strum::{Display, EnumString, EnumVariantNames};
use suppaftp::FtpStream;
use url::Url;

#[cfg(feature = "branch")]
pub use distro::Branch;
use distro::Mirror;
pub use distro::{MirrorList, MIRROR_LIST_FILE};
use distro::{
    BIG_TEST_FILE, BIG_TEST_FILE_DIR, DISTRO_NAME, MEDIUM_TEST_FILE, MEDIUM_TEST_FILE_DIR, REPO_ARCH, SMALL_TEST_FILE,
    SMALL_TEST_FILE_DIR,
};

#[cfg(feature = "continent")]
use super::geo::Continent;
#[cfg(feature = "country")]
use super::geo::Country;
use super::{FileSize, MeasureMethod, MultiParse, ShinyError, ShinyResult, BIN_NAME, USER_AGENT};

#[cfg_attr(feature = "arch", path = "distro/arch.rs")]
#[cfg_attr(feature = "manjaro", path = "distro/manjaro.rs")]
#[cfg_attr(feature = "artix", path = "distro/artix.rs")]
#[cfg_attr(feature = "rebornos", path = "distro/rebornos.rs")]
#[cfg_attr(feature = "endeavouros", path = "distro/endeavouros.rs")]
pub mod distro;

/// Protocol a [mirror][Mirror] can use
#[derive(Copy, Clone, Debug, Display, Serialize, Deserialize, PartialEq, Eq, EnumString, EnumVariantNames)]
#[serde(rename_all = "lowercase")]
#[strum(ascii_case_insensitive)]
pub enum Protocol {
    Http,
    Https,
    Ftp,
    Ftps,
    #[cfg(any(feature = "arch", feature = "artix"))]
    Rsync,
}

impl Protocol {
    fn get_most_secure_protocol(protocols: &[Self]) -> Self {
        let mut scheme = protocols[0];

        for &sch in protocols {
            match sch {
                Self::Https => {
                    if scheme != Self::Https {
                        scheme = sch;
                    }
                }
                Self::Ftps => {
                    if scheme == Self::Ftp || scheme == Self::Http {
                        scheme = sch;
                    }
                }
                Self::Http => {
                    if scheme == Self::Ftp {
                        scheme = sch;
                    }
                }
                _ => {}
            }
        }

        scheme
    }
}

impl MultiParse<Self> for Protocol {}

struct LocalMirrorList {
    local_mirrors: Vec<Url>,
    #[cfg(feature = "branch")]
    local_mirrorlist_branch: Branch,
}

#[derive(ValueEnum, Copy, Clone, Debug, Display, EnumVariantNames, EnumString, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[strum(ascii_case_insensitive)]
pub enum IPv {
    V4,
    V6,
}

impl MirrorList {
    pub fn download_mirrors() -> ShinyResult<Self> {
        let mut mirror_list = Self::pure_download_mirrors()?;

        #[cfg(feature = "protocol")]
        mirror_list.retain(|mirror| !mirror.protocols.is_empty());

        // Set the most secure protocol that is available
        #[cfg(feature = "protocol")]
        for mirror in mirror_list.iter_mut() {
            mirror.url = Url::parse(&format!("{}", mirror.url).replace(
                &format!("{}://", mirror.url.scheme()),
                &format!("{}://", Protocol::get_most_secure_protocol(&mirror.protocols)),
            ))?;
        }

        Ok(mirror_list)
    }

    /// Write the [`MirrorList`] to a local mirror list located at "/etc/pacman.d/mirrorlist"
    pub fn write_to_local_mirror_list(&self, #[cfg(feature = "branch")] branch: Branch) -> ShinyResult<()> {
        let mut template = format!(
            "##\n## {distro} Linux Mirror List\n## Generated on {datetime} by {name}\n##\n\n",
            distro = DISTRO_NAME,
            datetime = Local::now().format("%d-%m-%Y %H:%M:%S"),
            name = BIN_NAME
        );

        let mut mirror_list_file = OpenOptions::new()
            .write(true)
            .create(true)
            .truncate(true)
            .open(MIRROR_LIST_FILE)
            .map_err(|e| ShinyError::CouldntOpenLocalMirrorList(e.to_string()))?;

        for mirror in self.iter() {
            let mut url = mirror.url.clone();
            #[cfg(feature = "branch")]
            url.path_segments_mut()?
                .pop_if_empty()
                .push(&serde_plain::to_string(&branch)?)
                .extend(REPO_ARCH);
            #[cfg(not(feature = "branch"))]
            url.path_segments_mut()?.pop_if_empty().extend(REPO_ARCH);

            #[cfg(feature = "country")]
            template.push_str(&format!(
                "## {}{}\nServer = {}\n\n",
                if mirror.country == Country::Global {
                    String::new()
                } else {
                    format!("{}/", mirror.country.get_continent())
                },
                mirror.country,
                url,
            ));
            #[cfg(all(not(feature = "country"), feature = "continent"))]
            template.push_str(&format!("## {}\nServer = {url}\n\n", mirror.continent));
            #[cfg(all(not(feature = "country"), not(feature = "continent")))]
            template.push_str(&format!("Server = {url}\n\n"));
        }

        mirror_list_file.write_all(template.as_bytes())?;
        Ok(())
    }

    /// Print the status of mirrors in this list AND (Prints only for the ones that are in both lists) with the local mirrors
    pub fn print_status(#[cfg(feature = "branch")] settings_branch: Branch) -> ShinyResult<()> {
        let LocalMirrorList {
            local_mirrors,
            #[cfg(feature = "branch")]
            local_mirrorlist_branch,
        } = Self::get_urls_of_local_mirrors_and_branch()?;

        let mut mirror_list = Self::download_mirrors()?;

        // Print status of only mirrors which are in the local mirror list
        mirror_list.retain(|mirror1| local_mirrors.iter().any(|mirror2| mirror2.domain() == mirror1.url.domain()));

        #[cfg(feature = "country")]
        let longest_location_string = mirror_list
            .iter()
            .max_by(|mirror1, mirror2| mirror1.country.to_string().len().cmp(&mirror2.country.to_string().len()))
            .map_or(1, |mirror| mirror.country.to_string().len());
        #[cfg(all(not(feature = "country"), feature = "continent"))]
        let longest_location_string = mirror_list
            .iter()
            .max_by(|mirror1, mirror2| mirror1.continent.to_string().len().cmp(&mirror2.continent.to_string().len()))
            .map_or(1, |mirror| mirror.continent.to_string().len());

        #[cfg(feature = "branch")]
        if local_mirrorlist_branch != settings_branch {
            println!(
                "The branch from settings doesn't match the branch in local mirror list!\nPlease run {name} refresh or {name} config -b\n{}",
                "Showing status for the local mirror list branch!".red(),
                name = BIN_NAME
            );
        }

        let max_length_number = local_mirrors.len().to_string().len();

        #[cfg(feature = "branch")]
        println!(
            "{}",
            format!("Local mirror list status for {local_mirrorlist_branch} branch:")
                .yellow()
                .bold()
        );
        for (number, (mirror, url)) in local_mirrors
            .into_iter()
            .map(|url| (mirror_list.iter().find(|mirror| mirror.url.domain() == url.domain()), url))
            .enumerate()
        {
            match mirror {
                Some(mirror) => {
                    // TODO: make this fn
                    let mut murl = mirror.url.clone();
                    #[cfg(not(feature = "branch"))]
                    murl.path_segments_mut()?.pop_if_empty().extend(REPO_ARCH);
                    #[cfg(feature = "branch")]
                    murl.path_segments_mut()?
                        .pop_if_empty()
                        .push(&serde_plain::to_string(&local_mirrorlist_branch)?)
                        .extend(REPO_ARCH);

                    let is_badly_formatted = murl.path() != url.path();

                    println!(
                        "Mirror #{number:<number_width$} {status}{last_sync}{location}{url}{problem}",
                        number = number + 1,
                        number_width = max_length_number,
                        status = {
                            #[cfg(any(feature = "status", feature = "last_sync"))]
                            {
                                format_args!(
                                    " {:<2} ",
                                    match mirror.get_status(
                                        #[cfg(feature = "branch")]
                                        local_mirrorlist_branch
                                    ) {
                                        Status::Good => "OK".bright_green().bold(),
                                        _ => "--".red().bold(),
                                    }
                                )
                            }
                            #[cfg(all(not(feature = "status"), not(feature = "last_sync")))]
                            {
                                ""
                            }
                        },
                        last_sync = {
                            #[cfg(feature = "last_sync")]
                            {
                                format_args!(
                                    " {:>5} ",
                                    match mirror.last_sync {
                                        Some(v) => {
                                            if v > Duration::from_secs(3_600 * 100) {
                                                format!("{}h", v.as_secs() / 3_600).red().to_string()
                                            } else {
                                                format!("{:02}:{:02}", v.as_secs() / 3_600, (v.as_secs() % 3_600) / 60)
                                            }
                                        }
                                        None => "-----".red().bold().to_string(),
                                    }
                                )
                            }
                            #[cfg(not(feature = "last_sync"))]
                            {
                                ""
                            }
                        },
                        location = {
                            #[cfg(any(feature = "country", feature = "continent"))]
                            {
                                format_args!(
                                    " {location:<longest_location_string$}  ",
                                    location = {
                                        #[cfg(all(not(feature = "country"), feature = "continent"))]
                                        {
                                            mirror.continent
                                        }
                                        #[cfg(feature = "country")]
                                        {
                                            mirror.country
                                        }
                                    },
                                    longest_location_string = longest_location_string
                                )
                            }

                            #[cfg(all(not(feature = "country"), not(feature = "continent")))]
                            {
                                ""
                            }
                        },
                        url = if is_badly_formatted { url } else { mirror.url.clone() },
                        problem = if is_badly_formatted {
                            " Wrong path!".red().bold().to_string()
                        } else {
                            String::new()
                        }
                    );
                }
                None => {
                    // FIXME: THIS DO BETTER
                    #[cfg(any(feature = "country", feature = "continent"))]
                    println!(
                        "{mirror}  {status:<2}  {last_sync:>5}  {location:longest_location_string$}  {url} {invalid}",
                        mirror = format!(
                            "Mirror #{number:<number_width$}",
                            number = number + 1,
                            number_width = max_length_number,
                        )
                        .red(),
                        status = "--".red().bold(),
                        last_sync = "-----".red().bold(),
                        location = "-".repeat(longest_location_string).red().bold(),
                        longest_location_string = longest_location_string,
                        url = url,
                        invalid = "Doesn't exist!".red().bold(),
                    );
                    #[cfg(not(any(feature = "country", feature = "continent")))]
                    println!(
                        "{mirror}  {status:<2}  {last_sync:>5} {url} {invalid}",
                        mirror = format!(
                            "Mirror #{number:<number_width$}",
                            number = number + 1,
                            number_width = max_length_number,
                        )
                        .red(),
                        status = "--".red().bold(),
                        last_sync = "-----".red().bold(),
                        url = url,
                        invalid = "Doesn't exist!".red().bold(),
                    );
                }
            }
        }

        Ok(())
    }

    /// Read the urls of local mirrors saved in "/etc/pacman.d/mirrorlist"
    fn get_urls_of_local_mirrors_and_branch() -> ShinyResult<LocalMirrorList> {
        let mut file = OpenOptions::new()
            .read(true)
            .write(false)
            .open(MIRROR_LIST_FILE)
            .map_err(|_| ShinyError::NoLocalMirrorListFile)?;

        let mut string = String::new();
        file.read_to_string(&mut string)?;

        if string.is_empty() {
            return Err(ShinyError::LocalMirrorListFileEmpty);
        }

        #[cfg(feature = "branch")]
        let local_mirrorlist_branch = Branch::get_branch_from_local_mirror_list_string(&string)?;

        let local_mirrors: Vec<Url> = string
            .lines()
            .filter_map(|mut line| {
                if line.starts_with("Server = ") {
                    line = line.trim();
                    line = line.trim_start_matches("Server = ");
                    // line = line.trim_end_matches(REPO_ARCH);
                    line.parse().ok()
                } else {
                    None
                }
            })
            .collect();

        if local_mirrors.is_empty() {
            return Err(ShinyError::NoMirrorsInLocalMirrorList);
        }

        Ok(LocalMirrorList {
            local_mirrors,
            #[cfg(feature = "branch")]
            local_mirrorlist_branch,
        })
    }

    /// Filter by [domain][String], for example "mirror.example.com", it needs subdomain and domain
    pub fn filter_domains(&mut self, blocklist: &[String]) {
        self.retain(|mirror| !blocklist.contains(&mirror.url.domain().unwrap_or_default().to_string()));
    }

    /// Filter by [IP version][IPv]
    pub fn filter_ipv(&mut self, ipv: IPv) {
        #[cfg(not(feature = "native_ipv"))]
        self.retain(|mirror| {
            mirror
                .url
                .socket_addrs(|| match mirror.url.scheme() {
                    "https" => Some(443),
                    "ftp" | "ftps" => Some(21),
                    _ => Some(80),
                })
                .map(|s| {
                    s.iter().any(|x| match ipv {
                        IPv::V4 => x.is_ipv4(),
                        IPv::V6 => x.is_ipv6(),
                    })
                })
                .unwrap_or(false)
        });

        #[cfg(feature = "native_ipv")]
        self.retain(|mirror| match ipv {
            IPv::V6 => mirror.ipv6,
            IPv::V4 => mirror.ipv4,
        });
    }

    /// Filter by [continent][Continent]
    #[cfg(any(feature = "continent", feature = "country"))]
    pub fn filter_continent(&mut self, continent: Continent) {
        #[cfg(feature = "country")]
        {
            let continents_list = [continent.get_countries(), &[Country::Global]].concat();
            self.retain(|mirror| continents_list.contains(&mirror.country));
        }

        #[cfg(all(not(feature = "country"), feature = "continent"))]
        {
            self.retain(|mirror| mirror.continent == continent);
        }
    }

    /// Filter by [country/countries][Country]
    #[cfg(feature = "country")]
    pub fn filter_country(&mut self, countries: &[Country]) {
        self.retain(|mirror| countries.contains(&mirror.country) || mirror.country == Country::Global);
    }

    /// Filter the [mirrors][Mirror] by available [protocols][Protocol]
    #[cfg(feature = "protocol")]
    pub fn filter_protocol(&mut self, protocols: &[Protocol]) -> ShinyResult<()> {
        self.retain(|mirror| protocols.iter().any(|protocol| mirror.protocols.contains(protocol)));

        for mirror in self.iter_mut() {
            mirror.protocols.retain(|protocol| protocols.contains(protocol));
            // NOTE: Must do this because Url crate says ftps doesn't exist, which is right but... keeping the old code below if ftps is discarded in future
            mirror.url = Url::parse(&format!("{}", mirror.url).replace(
                &format!("{}://", mirror.url.scheme()),
                &format!("{}://", Protocol::get_most_secure_protocol(&mirror.protocols)),
            ))?;
            // if mirror.url.set_scheme(&mirror.protocols[0].to_string().to_lowercase()).is_err() {
            // 	bail!("Couldn't change the scheme for the url: {}!{:?}", mirror.url, mirror.protocols);
            // };
        }

        Ok(())
    }

    /// Filter fastest [mirrors][Mirror]
    pub fn filter_top_fastest_mirrors(
        &mut self,
        #[cfg(feature = "branch")] branch: Branch,
        amount: Option<usize>,
        timeout: Option<Duration>,
        file_size: Option<FileSize>,
        measure_method: Option<MeasureMethod>,
        rank_limit: Option<usize>,
    ) -> ShinyResult<()> {
        // NOTE: Remove mirrors with only RSYNC protocol as shiny-mirrors does not support it
        #[cfg(all(feature = "protocol", any(feature = "arch", feature = "artix")))]
        self.retain(|mirror| !(mirror.protocols.len() == 1 && mirror.protocols.contains(&Protocol::Rsync)));

        if let Some(rank_limit) = rank_limit {
            #[cfg(feature = "last_sync")]
            self.sort_by_key(|m| m.last_sync);
            self.truncate(rank_limit);
        }

        #[cfg(feature = "country")]
        self.sort_by_key(|m| m.country);
        #[cfg(all(not(feature = "country"), feature = "continent"))]
        self.sort_by_key(|m| m.continent);

        self.rank_mirrors(
            #[cfg(feature = "branch")]
            branch,
            timeout,
            file_size,
            measure_method,
        )?; // Measure speed connection to the mirror and save it
        self.retain(|mirror| mirror.time.is_some()); // Removing the ones that couldn't be ranked

        if !self.is_empty() {
            // If there is more than 1 mirrors get average mirror time and remove which are over average time
            if self.len() > 1 {
                // Get average duration
                let mean = {
                    let sum: Duration = self.iter().filter_map(|m| m.time).sum();
                    let secs = sum.as_secs_f64() / self.len() as f64;
                    Duration::from_secs_f64(secs)
                };
                println!("The average Mirror Time was {mean:.0?}");

                // Remove mirrors with time over average time
                // 200ms margin error if MeasureMethod::Total
                // 75ms margin error if MeasureMethod::Transfer
                let mean_time_with_margin_error = if measure_method == Some(MeasureMethod::Transfer) {
                    Duration::from_millis(75).add(mean)
                } else {
                    Duration::from_millis(200).add(mean)
                };
                self.retain(|mirror| mirror.time <= Some(mean_time_with_margin_error));
            }
            self.sort_by(|mirror1, mirror2| mirror1.time.cmp(&mirror2.time)); // Sort by the speed connection to the mirror

            if let Some(amount) = amount {
                self.truncate(amount);
            }
        }
        Ok(())
    }

    /// Filter out outdated mirrors by status of [`Branch`] and [`last_sync`][Duration]
    /// When [`updated_only`][bool] is [True][bool] it will ignore [`last_sync`][Duration] and just get updated mirror for the supplied [branch][Branch]
    #[cfg(any(feature = "status", feature = "last_sync"))]
    pub fn filter_status(&mut self, #[cfg(feature = "branch")] branch: Branch, updated_only: bool, last_sync: Option<Duration>) {
        let last_sync = last_sync.unwrap_or(Duration::from_secs(86_400));

        self.retain(|mirror| {
            mirror.get_status(
                #[cfg(feature = "branch")]
                branch,
            ) == Status::Good
                || if updated_only {
                    false
                } else {
                    mirror.last_sync.map_or(false, |sync| sync <= last_sync)
                }
        });
    }

    /// Filter okay [mirrors][Mirror] without any settings
    pub fn get_perfect_mirrors(&mut self, backup: Self) -> ShinyResult<()> {
        #[cfg(feature = "country")]
        let country = Country::find_current_country().unwrap_or(Country::Unknown);
        #[cfg(feature = "country")]
        {
            // let country = Country::find_current_country().unwrap_or(Country::Unknown);

            self.filter_country(&[country]);

            if self.iter().filter(|x| x.country != Country::Global).count() > 0 {
                return Ok(());
            }

            println!("Couldn't get any mirrors based on country gotten by the geolocation.\nGetting mirrors based on a continent which was acquired by geolocation.");
        }

        #[cfg(all(feature = "country", feature = "continent"))]
        {
            *self = backup;
        }

        #[cfg(feature = "continent")]
        {
            #[cfg(all(feature = "country", feature = "continent"))]
            self.filter_continent(if country != Country::Unknown {
                country.get_continent()
            } else {
                Continent::find_current_continent()?
            });

            #[cfg(all(not(feature = "country"), feature = "continent"))]
            self.filter_continent(Continent::find_current_continent()?);

            if !self.is_empty() {
                return Ok(());
            }
        }

        Err(ShinyError::NoMirrorsFound)
    }

    /// Try downloading test file from every [mirrors][Mirror] and save it's download ["speed"][Duration]
    fn rank_mirrors(
        &mut self,
        #[cfg(feature = "branch")] branch: Branch,
        timeout: Option<Duration>,
        file_size: Option<FileSize>,
        measure_method: Option<MeasureMethod>,
    ) -> ShinyResult<()> {
        let timeout = timeout.unwrap_or(Duration::from_secs(3));
        let measure_method = measure_method.unwrap_or(MeasureMethod::Total);
        let client = reqwest::blocking::Client::builder()
            .user_agent(USER_AGENT)
            .timeout(timeout)
            .build()?;

        let file_size = file_size.unwrap_or(FileSize::Small); // When used as a lib

        let (test_file_dir, test_file) = match file_size {
            FileSize::Small => (SMALL_TEST_FILE_DIR, SMALL_TEST_FILE),
            FileSize::Medium => (MEDIUM_TEST_FILE_DIR, MEDIUM_TEST_FILE),
            FileSize::Big => (BIG_TEST_FILE_DIR, BIG_TEST_FILE),
        };

        let arch = match ARCH {
            "arm" => "aarch64",
            _ => ARCH,
        };

        #[cfg(feature = "country")]
        let longest_country = self
            .iter()
            .max_by(|mirror1, mirror2| mirror1.country.to_string().len().cmp(&mirror2.country.to_string().len()))
            .map_or(0, |mirror| mirror.country.to_string().len());
        #[cfg(all(not(feature = "country"), feature = "continent"))]
        let longest_continent = self
            .iter()
            .max_by(|mirror1, mirror2| mirror1.continent.to_string().len().cmp(&mirror2.continent.to_string().len()))
            .map_or(0, |mirror| mirror.continent.to_string().len());

        let mut stdout = stdout();
        for mirror in self.iter_mut() {
            #[cfg(feature = "country")]
            print!(
                "{time:>6} {country:<longest_country$} : {url}",
                time = ".....",
                country = mirror.country,
                url = mirror.url,
            );
            #[cfg(all(not(feature = "country"), feature = "continent"))]
            print!(
                "{time:>6} {continent:<longest_continent$} : {url}",
                time = ".....",
                continent = mirror.continent,
                url = mirror.url,
            );
            #[cfg(not(any(feature = "country", feature = "continent")))]
            print!("{time:>6} : {url}", time = ".....", url = mirror.url);
            stdout.flush()?;

            let response = match Protocol::from_str(mirror.url.scheme())? {
                Protocol::Http | Protocol::Https => http_download(
                    &client,
                    mirror,
                    #[cfg(feature = "branch")]
                    branch,
                    arch,
                    &test_file_dir,
                    test_file,
                ),
                Protocol::Ftp | Protocol::Ftps => ftp_download(
                    mirror,
                    timeout,
                    #[cfg(feature = "branch")]
                    branch,
                    arch,
                    &test_file_dir,
                    test_file,
                ),
                #[cfg(any(feature = "arch", feature = "artix"))]
                _ => return Err(ShinyError::NoRsyncSupport),
            };

            mirror.time = response.ok().map(|(request_duration, transfer_duration)| match measure_method {
                MeasureMethod::Total => request_duration.add(transfer_duration),
                MeasureMethod::Transfer => transfer_duration,
            });

            println!(
                "\r{:>6}",
                match mirror.time {
                    Some(time) => format!("{time:>5.0?}").bright_green().bold(),
                    None => "-----".red().bold(),
                }
            );
            stdout.flush()?;
        }

        Ok(())
    }
}

fn http_download(
    client: &reqwest::blocking::Client,
    mirror: &mut Mirror,
    #[cfg(feature = "branch")] branch: Branch,
    arch: &str,
    test_file_dir: &[&str],
    test_file: &str,
) -> ShinyResult<(Duration, Duration)> {
    let request_now = Instant::now();
    let mut url = mirror.url.clone();
    #[cfg(all(feature = "branch", not(feature = "rebornos")))]
    url.path_segments_mut()?
        .pop_if_empty()
        .push(&serde_plain::to_string(&branch)?)
        .extend(test_file_dir)
        .push(arch)
        .push(test_file);

    #[cfg(all(not(feature = "branch"), not(feature = "rebornos")))]
    url.path_segments_mut()?
        .pop_if_empty()
        .extend(test_file_dir)
        .push(arch)
        .push(test_file);

    #[cfg(feature = "rebornos")]
    url.path_segments_mut()?.pop_if_empty().push(test_file);

    let response = client.get(url.as_str()).send()?;

    let request_duration = request_now.elapsed();

    let transfer_now = Instant::now();
    let _bytes = response.bytes()?;
    let transfer_duration = transfer_now.elapsed();

    Ok((request_duration, transfer_duration))
}

fn ftp_download(
    mirror: &mut Mirror,
    timeout: Duration,
    #[cfg(feature = "branch")] branch: Branch,
    arch: &str,
    test_file_dir: &[&str],
    test_file: &str,
) -> ShinyResult<(Duration, Duration)> {
    let socket = mirror.url.socket_addrs(|| Some(21))?;
    let socket = match socket.into_iter().find(SocketAddr::is_ipv4) {
        Some(v) => v,
        None => return Err(ShinyError::NoIPv4),
    };

    let request_now = Instant::now();
    let mut ftp_stream = FtpStream::connect(socket)?;
    ftp_stream.get_ref().set_read_timeout(Some(timeout))?;
    ftp_stream.get_ref().set_write_timeout(Some(timeout))?;
    ftp_stream.login("anonymous", "anonymous@shiny.org")?;
    #[cfg(feature = "branch")]
    ftp_stream.cwd(&format!(
        "/{}/{}/{}/{}",
        mirror.url.path(),
        serde_plain::to_string(&branch)?,
        test_file_dir.join("/"),
        arch
    ))?;
    #[cfg(not(feature = "branch"))]
    ftp_stream.cwd(&format!("/{}/{}/{arch}", mirror.url.path(), test_file_dir.join("/")))?;
    let request_duration = request_now.elapsed();

    let transfer_now = Instant::now();
    ftp_stream.retr_as_buffer(test_file)?;
    let transfer_duration = transfer_now.elapsed();
    ftp_stream.quit()?;

    Ok((request_duration, transfer_duration))
}

#[repr(i8)]
#[derive(Default, Copy, Clone, Debug, Serialize_repr, Deserialize_repr, PartialEq, Eq)]
pub enum Status {
    #[default]
    Unknown = -1,
    Bad = 0,
    Good = 1,
}
